document.addEventListener("DOMContentLoaded", function () {
  const burger = document.querySelector(".burger");
  const nav = document.querySelector(".nav");

  burger.addEventListener("click", function () {
    burger.classList.toggle("burger-active");
    nav.classList.toggle("nav-click");
  });

  const navLinks = document.querySelectorAll(".nav a");
  for (const link of navLinks) {
    link.addEventListener("click", function () {
      if (nav.classList.contains("nav-click")) {
        burger.classList.remove("burger-active");
        nav.classList.remove("nav-click");
      }
    });
  }
});
