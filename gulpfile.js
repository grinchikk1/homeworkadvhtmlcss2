import gulp from "gulp";
import dartSass from "sass";
import gulpSass from "gulp-sass";
import cleanCSS from "gulp-clean-css";
import clean from "gulp-clean";
import concat from "gulp-concat";
import autoprefixer from "gulp-autoprefixer";
import uglify from "gulp-uglify";
import browserSync from "browser-sync";
import imagemin from "gulp-imagemin";

const scss = gulpSass(dartSass);

// очищення папки dist
function cleanDist() {
  return gulp.src("dist/").pipe(clean());
}

// компіляція scss файлів у css та додавання вендорних префіксів
function styles() {
  return gulp
    .src("src/scss/style.scss")
    .pipe(scss())
    .pipe(autoprefixer({ overrideBrowserslist: ["last 2 versions"] }))
    .pipe(concat("style.min.css"))
    .pipe(cleanCSS())
    .pipe(gulp.dest("dist/css"))
    .pipe(browserSync.stream());
}

// конкатенація та мініфікація js файлів
function scripts() {
  return gulp
    .src("src/js/main.js")
    .pipe(concat("scripts.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("dist/js"))
    .pipe(browserSync.stream());
}

// копіювання файлу html
function htmls() {
  return gulp
    .src("index.html")
    .pipe(gulp.dest("dist/"))
    .pipe(browserSync.stream());
}

// оптимізація картинок
function images() {
  return gulp
    .src("src/img/**/*.{jpg,png,webp,jpeg,svg,gif}")
    .pipe(imagemin())
    .pipe(gulp.dest("dist/img"))
    .pipe(browserSync.stream());
}

// відстеження змін файлів проєкту
async function watcher() {
  gulp.watch("src/scss/**/*.scss", styles);
  gulp.watch("src/js/**/*.js", scripts);
  gulp.watch("src/img/**/*.*", images);
  gulp.watch("index.html", htmls);
}

// запуск сервера
function server() {
  browserSync.init({
    server: {
      baseDir: "./dist",
    },
    port: 3000,
  });
}

const _dev = gulp.series(
  cleanDist,
  gulp.parallel(styles, scripts, images, htmls),
  gulp.parallel(server, watcher)
);

const _build = gulp.series(
  cleanDist,
  gulp.parallel(styles, scripts, images, htmls)
);

export { _dev as dev };

export { _build as build };

const _default = gulp.series(_dev);
export { _default as default };
